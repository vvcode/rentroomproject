﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentRoom.Model
{
    public class SettingGroupParameter
    {
        public SettingGroupParameter()
        {
            settingParameterList = new List<SettingParameter>();
            Url = string.Empty;
        }

        public string Name { get; set; }
        public IList<SettingParameter> settingParameterList { get; set; }
        public string Url { get; set; }
    }
}
