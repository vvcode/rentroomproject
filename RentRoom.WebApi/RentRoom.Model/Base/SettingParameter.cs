﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentRoom.Model
{
    public class SettingParameter
    {
        public string Id { get; set; }
        public string Name  { get; set; }
        public string Description  { get; set; }
        public string Type  { get; set; }
        public string Value  { get; set; }
        public bool Required { get; set; }
    }
}
