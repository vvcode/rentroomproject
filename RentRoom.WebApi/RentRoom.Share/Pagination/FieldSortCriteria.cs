﻿/*
 * Author: Zoran Maksimovic
 * Date: 16.04.2012
 * 
 * http://www.agile-code.com
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RentRoom.Share
{
    //-----------------------------------------------------------------------
    /// <summary>
    /// Represents a sort expression where a property of the sequence item is sorted upon.
    /// Useful to avoid case statement when doing "simple" sorts by "simple" property.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FieldSortCriteria<T> : ISortCriteria<T> where T : class
    {
        //-----------------------------------------------------------------------
        public String Name { get; set; }

        //-----------------------------------------------------------------------
        public SortDirections Direction { get; set; }

        //-----------------------------------------------------------------------
        public FieldSortCriteria()
        {
            this.Direction = SortDirections.Ascending;
        }

        //-----------------------------------------------------------------------
        public FieldSortCriteria(String name, SortDirections direction)
            : base()
        {
            Name = name;
            Direction = direction;
        }

        //-----------------------------------------------------------------------
        public IOrderedQueryable<T> ApplyOrdering(IQueryable<T> qry, Boolean useThenBy)
        {
            IOrderedQueryable<T> result = null;
            var descending = this.Direction == SortDirections.Descending;
            result = !useThenBy ? qry.OrderBy(Name, descending) : qry.ThenBy(Name, descending);
            return result;
        }
    }
}
