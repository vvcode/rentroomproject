﻿/*
 * Author: Zoran Maksimovic
 * Date: 16.04.2012
 * 
 * http://www.agile-code.com
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RentRoom.Share
{
    public enum SortDirections
    {
        Ascending = 0,
        Descending = 1
    }

    //-----------------------------------------------------------------------
    /// <summary>
    /// Common interface to the sort implementations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISortCriteria<T>
    {
        //-----------------------------------------------------------------------
        SortDirections Direction { get; set; }

        //-----------------------------------------------------------------------
        IOrderedQueryable<T> ApplyOrdering(IQueryable<T> query, Boolean useThenBy);
    }
}
