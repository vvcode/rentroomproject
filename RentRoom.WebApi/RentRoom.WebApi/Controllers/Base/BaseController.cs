﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using RentRoom.Infrastructure.EF;
using RentRoom.Model;

namespace RentRoom.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        public const int DefaultPageSize = 10;
        public const int DefaultPage = 1;

        private UnitOfWork unitOfWork { get; set; }

        public UnitOfWork UnitOfWorkManager
        {
            get
            {
                return unitOfWork;
            }
        }

        public  BaseController()
        {
            if (unitOfWork == null)
            {
                unitOfWork = new UnitOfWork();
            }
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}