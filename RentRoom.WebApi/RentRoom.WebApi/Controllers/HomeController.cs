﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using RentRoom.Infrastructure.EF;
using RentRoom.Model;
using RentRoom.Share;
using RentRoom.WebApi.Helper;

namespace RentRoom.WebApi.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        [Route("api/Info")]
        public HttpResponseMessage GetInfo(string name = "", bool onlyGroup = true)
        {
            IList<SettingGroupParameter> settingGroupParameterList = new List<SettingGroupParameter>();

            if (name != "")
                onlyGroup = false;

            settingGroupParameterList.Add(new SettingGroupParameter() { Name = "page", Url = "api/info", settingParameterList = SettingParameterHelp.LoadPageParameter(onlyGroup) });
            settingGroupParameterList.Add(new SettingGroupParameter() { Name = "eventlog", Url = "api/eventlog" });

            if (name == "")
                return Request.CreateResponse(HttpStatusCode.OK, settingGroupParameterList);
            else
                return Request.CreateResponse(HttpStatusCode.OK, settingGroupParameterList.Where(x => x.Name == name));
        }

        [Route("api/EventLog")]
        public HttpResponseMessage GetEventLog(int top = DefaultPageSize
                                               , int page = DefaultPage
                                               , int id = 0
                                               , string orderby = "RequestTimestamp"
                                               , SortDirections sortDirections = SortDirections.Ascending
                                               , string includeProperties = ""
                                               , ContentType contentType = ContentType.TEXT
                                               , string property = "")
        {
            //create the query
            var query = new SearchQuery<ApiLogEntry>();
            query.AddFilter(x => x.Application == "RentRoom WebApi");

            if (id != 0)
                query.AddFilter(x => x.Id == id);

            query.AddSortCriteria(new FieldSortCriteria<ApiLogEntry>(orderby, sortDirections));


            //this is the same as the following
            query.Take = top;
            query.Skip = top * (page - 1);
            query.IncludeProperties = includeProperties;

            HttpResponseMessage resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
            var result = UnitOfWorkManager.ApiLogEntryRepository.Search(query);

            if (property == "")
                return Request.CreateResponse(HttpStatusCode.OK, result);
            else
            {
                var apiLogEntry = id == 0 ? result.Entities.ElementAt(0) : result.Entities.Where(x => x.Id == id).FirstOrDefault();
                var propertyResult = "";
                if (apiLogEntry != null)
                    propertyResult = apiLogEntry.GetType().GetProperty(property).GetValue(apiLogEntry, null) as string;

                return new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent(propertyResult == null ? "" : propertyResult, System.Text.Encoding.UTF8, contentType.ToValue()) };
            }

        }
    }
}