﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using RentRoom.Infrastructure.EF;
using RentRoom.Model;
using RentRoom.Share;

namespace RentRoom.WebApi.Controllers
{
    public class TestViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }

    [Authorize]
    public class TestController : BaseController
    {
        //http://www.agile-code.com/blog/entity-framework-code-first-filtering-and-sorting-with-paging-1/
        //http://www.agile-code.com/blog/entity-framework-code-first-filtering-and-sorting-with-paging-2/

        public HttpResponseMessage GetAllTests()
        {
            TestViewModel[] testModels = new TestViewModel[] 
                                            { 
                                                new TestViewModel { Id = 1, Email = "test1@test.com" }, 
                                                new TestViewModel { Id = 2, Email = "test2@test.com" }, 
                                                new TestViewModel { Id = 3, Email = "test3@test.com" } 
                                            };

            return Request.CreateResponse(HttpStatusCode.OK, testModels);
        }

        public HttpResponseMessage GetAllTestsQuery()
        {
            //create the query
            var query = new SearchQuery<ApiLogEntry>();
            query.AddFilter(x => x.Application == "RentRoom WebApi");
            query.AddSortCriteria(new FieldSortCriteria<ApiLogEntry>("RequestTimestamp", SortDirections.Ascending));
 

            //this is the same as the following
            //query.AddFilter(product => product.Name.StartsWith("Bred") && product.IsActive == true);
            query.Take = 2;
            query.Skip = 0;


            //execute the search.        
            var result = UnitOfWorkManager.ApiLogEntryRepository.Search(query);

            if (result != null)
            {
                Console.WriteLine("Total number of rows: " + result.Count);
                Console.WriteLine("Has more results?: " + result.HasNext.ToString());
                Console.WriteLine("Has previous results?: " + result.HasPrevious.ToString());

                result.Entities.ToList();
            }

            return GetAllTests();
        }
    }
}