﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentRoom.Model;

namespace RentRoom.WebApi.Helper
{
    public class SettingParameterHelp
    {
        public static IList<SettingParameter> LoadPageParameter(bool onlyGroup)
        {
            IList<SettingParameter> settingParameterList = new List<SettingParameter>();
            if (!onlyGroup)
            {
                settingParameterList.Add(new SettingParameter() { Id = "top", Name = "top", Description = "Represents the number of items to be returned by the query.", Required = false, Type = "int", Value = "10" });
                settingParameterList.Add(new SettingParameter() { Id = "page", Name = "page", Description = "Number of items to be skipped. Useful for paging.", Required = false, Type = "int", Value = "1" });
                settingParameterList.Add(new SettingParameter() { Id = "id", Name = "id", Description = "Filter data one record.", Required = false, Type = "int", Value = "1" });
                settingParameterList.Add(new SettingParameter() { Id = "orderby", Name = "orderby", Description = "Name of Property for sorting.", Required = false, Type = "string", Value = "RequestTimestamp" });
                settingParameterList.Add(new SettingParameter() { Id = "sortDirections", Name = "sortDirections", Description = "ASC or DESC for sorting.", Required = false, Type = "Enum", Value = "SortDirections {  Ascending = 0, Descending = 1 }" });
                settingParameterList.Add(new SettingParameter() { Id = "contentType", Name = "contentType", Description = "Content Type for reponse data.", Required = false, Type = "Enum", Value = "ContentType { JSON, TEXT }" });
                settingParameterList.Add(new SettingParameter() { Id = "property", Name = "property", Description = "Property for reponse data.", Required = false, Type = "string", Value = "ResponseContentBody" });
            }
            return settingParameterList;
        }
    }
}